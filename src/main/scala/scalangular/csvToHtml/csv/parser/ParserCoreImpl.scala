package scalangular.csvToHtml.csv.parser

import cats.data.State
import scalangular.csvToHtml.csv.model.CsvCell

class ParserCoreImpl extends ParserCore {

  private val unmodified: CsvParserCoreState = State[ParserState, Vector[CsvCell]] { state =>
    (state, state.cells)
  }

  override def evalCell(input: String)(implicit config: ParserConfig): CsvParserCoreState = {
    State[ParserState, Vector[CsvCell]] { parserState =>
      val nextCells = parserState.cells :+ CsvCell(input, parserState.x + 1, parserState.y)
      (parserState.copy(x = parserState.x + 1, cells = nextCells), nextCells)
    }
  }

  override def evalRow(input: String)(implicit config: ParserConfig): CsvParserCoreState = {
    config match {
      case BasicParserConfig(cellSeparator, _) =>
        val cellsRaw = input.split(cellSeparator)

        cellsRaw.foldLeft(unmodified)((state: CsvParserCoreState, current: String) => {
          state.flatMap(_ => evalCell(current)
          )
        })
    }
  }

  override def evalRows(input: String)(implicit config: ParserConfig): CsvParserCoreState = {
    config match {
      case BasicParserConfig(_, lineSeparator) =>
        val rowsRaw = input.split(lineSeparator)
        rowsRaw.foldLeft(unmodified)((state: CsvParserCoreState, current: String) => {
          state
            .flatMap(_ => State[ParserState, Vector[CsvCell]] { state =>
              (state.copy(y = state.y + 1, x = 0), state.cells)
            })
            .flatMap(_ => evalRow(current))
        })
    }
  }
}

object ParserCoreImpl {
  def apply(): ParserCoreImpl = new ParserCoreImpl()
}