package scalangular.csvToHtml.csv.parser

import cats.data.State
import scalangular.csvToHtml.csv.model.CsvCell

trait ParserCore {

  type CsvParserCoreState = State[ParserState, Vector[CsvCell]]

  def evalCell(input: String)(implicit config: ParserConfig): CsvParserCoreState

  def evalRow(input: String)(implicit config: ParserConfig): CsvParserCoreState

  def evalRows(input: String)(implicit config: ParserConfig): CsvParserCoreState
}

case class ParserState(x: Int, y: Int, cells: Vector[CsvCell])
