package scalangular.csvToHtml.csv.parser

sealed trait ParserConfig

final case class BasicParserConfig(cellSeparator: String, lineSeparator: String) extends ParserConfig
