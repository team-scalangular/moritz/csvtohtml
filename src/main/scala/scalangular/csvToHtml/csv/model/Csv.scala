package scalangular.csvToHtml.csv.model

import cats.Show
import cats.syntax.show._

sealed trait Csv

final case class CsvCell(content: String, x: Int, y: Int)

object CsvTable {

  implicit val showCsvCell: Show[CsvCell] = Show.show(cell => {
    cell.content
  })

  implicit val showCscCellVector: Show[Vector[CsvCell]] = Show.show((cells: Vector[CsvCell]) => {
    cells.map((cell: CsvCell) => cell.show).mkString("|")
  })

  def rowMap(cells: Vector[CsvCell]): Map[Int, Vector[CsvCell]] = {
    cells.groupBy(_.y)
  }

  def colMap(cells: Vector[CsvCell]): Map[Int, Vector[CsvCell]] = {
    cells.groupBy(_.x)
  }

  def print(cells: Vector[CsvCell]): String = {
    rowMap(cells)
      .map((row: (Int, Vector[CsvCell])) => s"${row._1}: ${row._2.show}\n")
      .foldLeft("")(_ + _)
  }
}
