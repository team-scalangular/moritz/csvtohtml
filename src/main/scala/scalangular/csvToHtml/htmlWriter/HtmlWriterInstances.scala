package scalangular.csvToHtml.htmlWriter

object HtmlWriterInstances {
  implicit val stringWriter: HtmlWriter[String] = (value: String) => value

  implicit def htmlPlainWriter(implicit w: HtmlWriter[String]): HtmlWriter[HtmlPlain] = (value: HtmlPlain) => {
    w.write(value.content)
  }

  implicit val tagWriter: HtmlWriter[HtmlTag] = new HtmlWriter[HtmlTag] {
    override def write(value: HtmlTag): String = matchAndCall(value)
  }

  implicit def tableRowWriter: HtmlWriter[TableRow] = new HtmlWriter[TableRow] {
    override def write(value: TableRow): String = {
      value.content.foldLeft("<tr>")((s: String, a: HtmlTag) => {
        val res = a match {
          case c: TableRow => write(c)
          case _ => matchAndCall(a)
        }

        s + res
      }) + "</tr>"
    }
  }

  implicit val tableHeaderWriter: HtmlWriter[TableHeaderCell] = new HtmlWriter[TableHeaderCell] {
    override def write(value: TableHeaderCell): String = {
      value.content.foldLeft("<th>")((s: String, a: HtmlTag) => {
        val res = a match {
          case c: TableHeaderCell => write(c)
          case _ => matchAndCall(a)
        }

        s + res
      }) + "</th>"
    }
  }

  implicit val tableCelWriter: HtmlWriter[TableCell] = new HtmlWriter[TableCell] {
    override def write(value: TableCell): String = {
      value.content.foldLeft("<td>")((s: String, a: HtmlTag) => {
        val res = a match {
          case c: TableCell => write(c)
          case _ => matchAndCall(a)
        }

        s + res
      }) + "</td>"
    }
  }

  implicit val tableWriter: HtmlWriter[Table] = new HtmlWriter[Table] {
    override def write(value: Table): String = {
      value.content.foldLeft("<table>")((s: String, a: HtmlTag) => {
        val res = a match {
          case c: Table => write(c)
          case _ => matchAndCall(a)
        }

        s + res
      }) + "</table>"
    }
  }

  private def matchAndCall(a: HtmlTag): String = {
    a match {
      case x: Table => HtmlTag.toHtml(x)
      case x: TableRow => HtmlTag.toHtml(x)
      case x: TableHeaderCell => HtmlTag.toHtml(x)
      case x: TableCell => HtmlTag.toHtml(x)
      case HtmlPlain(content) => HtmlTag.toHtml(content)
    }
  }
}
