package scalangular.csvToHtml.htmlWriter

sealed trait HtmlTag

final case class Table(content: List[HtmlTag]) extends HtmlTag

final case class TableRow(content: List[HtmlTag]) extends HtmlTag

final case class TableHeaderCell(content: List[HtmlTag]) extends HtmlTag

final case class TableCell(content: List[HtmlTag]) extends HtmlTag

final case class HtmlPlain(content: String) extends HtmlTag

object HtmlTag {
  def toHtml[A](value: A)(implicit w: HtmlWriter[A]): String = w.write(value)
}