package scalangular.csvToHtml.htmlWriter

trait HtmlWriter[A] {
  def write(value: A): String
}
