package scalangular.csvToHtml.csvReader

import scalangular.csvToHtml.csv.model.CsvTable
import scalangular.csvToHtml.csv.parser.{BasicParserConfig, ParserCoreImpl, ParserState}
import scalangular.csvToHtml.htmlWriter._

import scala.io.Source

class CsvToHtmlParser {

  import cats.data.State

  type HtmlInputState = State[List[HtmlTag], Table]

  private val unmodified: State[List[HtmlTag], Table] = State[List[HtmlTag], Table] { state =>
    (state, Table(state))
  }

  def evalCell(input: String, isHeader: Boolean = false): HtmlInputState = {
    State[List[HtmlTag], Table] { elements =>
      val res: List[HtmlTag] = if (isHeader) {
        TableHeaderCell(List(HtmlPlain(input))) :: elements
      } else {
        elements :+ TableCell(List(HtmlPlain(input)))
      }

      (res, Table(res))
    }
  }

  def evalLine(input: String, isHeaderLine: Boolean = false): HtmlInputState = {
    val cells = input.split(",")
    println(s"Evaling $input")

    cells.foldRight(unmodified)((cell: String, tags: State[List[HtmlTag], Table]) => {
      tags.flatMap(_ => evalCell(cell, isHeaderLine))
    }).flatMap(_ => State[List[HtmlTag], Table] { elements: List[HtmlTag] =>
      val res = List(TableRow(elements))
      (res, Table(res))
    })
  }

  def evalLines(input: List[String]): State[List[HtmlTag], Table] = {
    input.foldRight(unmodified)((line: String, tags: State[List[HtmlTag], Table]) => {
      tags.flatMap(_ => evalLine(line))
    })
  }

  def evalFile(path: String): Table = {
    val lines = Source.fromFile(path).getLines.toList

    val res = for {
      _ <- evalLine(lines.head, isHeaderLine = true)
      res <- evalLines(lines.tail)
    } yield res

    res.runA(Nil).value
  }

  def saveAsHtmlFile(tag: HtmlTag, path: String): String = {
    import scalangular.csvToHtml.htmlWriter.HtmlWriterInstances._

    val htmlString = HtmlTag.toHtml(tag)

    import java.io.PrintWriter

    new PrintWriter(s"$path.html") {
      write(htmlString);
      close()
    }

    path
  }
}

object CsvToHtmlParser {
  def apply(): CsvToHtmlParser = new CsvToHtmlParser()
}

object Test extends App {
  val r = ParserCoreImpl()
  val input = "Region,Country,Item Type,Sales Channel,Order Priority,Order Date,Order ID,Ship Date,Units Sold,Unit " +
    "Price,Unit Cost,Total Revenue,Total Cost,Total Profit\nAustralia and Oceania,Tuvalu,Baby Food,Offline,H," +
    "5/28/2010,669165933,6/27/2010,9925,255.28,159.42,2533654,1582243.5,951410.5"

  implicit val p: BasicParserConfig = BasicParserConfig(",", "\n")

  val res = r.evalRows(input)
  val rowMap = res.runA(ParserState(0, 0, Vector.empty)).value
  println(CsvTable.print(rowMap))
}